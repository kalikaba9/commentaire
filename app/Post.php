<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    
   /* public function comments()
    {
        return $this->morphMany(Comment::class, 'commentable')->whereNull('parent_id');
    }*/

    
    
     

    public function user()
    {
        return $this->belongsTo(User::class);
    }
    public function comments()
    {
        return $this->morphMany(Comment::class, 'commentable')->whereNull('parent_id');
       // return $this->hasMany(Comment::class)->whereNull('parent_id');
    }
}
