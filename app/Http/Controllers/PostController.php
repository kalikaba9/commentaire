<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
//use App\Comment;
use App\Post;

class PostController extends Controller
{
    public function __construct()
    {
        return $this->middleware('auth');
    }

    public function create()
    {
        return view('post');
    }

    public function store(Request $request)
    {
        $post =  new Post;
        $post->title = $request->get('title');
        $post->body = $request->get('body');
        $post->image = $request->get('image');
        /*$image_name = rand() . '.' . $image->getClientOriginalExtension();
        $image->move(public_path('images'), $image_name);*/

        
        $image = $request->file('image');

     
        
        

        $post->save();

        return redirect('posts');

    }

    public function index()
{
    $posts = Post::all();

    return view('index', compact('posts'));
}


public function show($id)
{
    $post = Post::find($id);

    return view('show', compact('post'));
}

}
